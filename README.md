# Extras

In order to work, you might (and probably will) have to configure your application server.
For WildFly, I needed to activate the "default" authentication mechanism using `jboss-cli`:

```
/subsystem=undertow/application-security-domain=other:add(security-domain=ApplicationDomain, integrated-jaspi=false)
```
