package com.realdolmen.jcc.beans;

import com.realdolmen.jcc.config.JccJdbcIdentityStore;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class RegistrationBean {

    @Inject
    private JccJdbcIdentityStore identityStore;

    private String username;
    private String password;
    private String confirm;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String submit(){
        identityStore.registerNew(this);
        return "/index.xhtml?faces-redirect=true";
    }
}
