package com.realdolmen.jcc.config;

import com.realdolmen.jcc.beans.Callers;
import com.realdolmen.jcc.beans.RegistrationBean;
import com.realdolmen.jcc.persistence.CallersRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;

@ApplicationScoped
public class JccJdbcIdentityStore {
    @Inject
    private Pbkdf2PasswordHash passwordHash;

    @Inject
    private CallersRepository callersRepository;

    public void registerNew(RegistrationBean registrationBean) {
        String hashed = passwordHash.generate(registrationBean.getPassword().toCharArray());
        Callers user = new Callers(
                registrationBean.getUsername(),
                hashed,
                "USER"
        );
        callersRepository.save(user);
    }
}
