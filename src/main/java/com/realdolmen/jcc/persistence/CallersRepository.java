package com.realdolmen.jcc.persistence;

import com.realdolmen.jcc.beans.Callers;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class CallersRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(Callers callers){
        entityManager.persist(callers);
    }

    public Callers getById(Integer id){
        return entityManager.find(Callers.class, id);
    }
}
