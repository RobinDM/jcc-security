package com.realdolmen.jcc;

import javax.enterprise.context.ApplicationScoped;
import javax.security.enterprise.authentication.mechanism.http.FormAuthenticationMechanismDefinition;
import javax.security.enterprise.authentication.mechanism.http.LoginToContinue;
import javax.security.enterprise.identitystore.DatabaseIdentityStoreDefinition;

@DatabaseIdentityStoreDefinition(dataSourceLookup = "java:/rd/RealDolmenDataSource",
                                 priority = 3,
                                 callerQuery = "select password from callers where username = ?",
                                 groupsQuery = "select role from callers where username = ?")
@FormAuthenticationMechanismDefinition(loginToContinue = @LoginToContinue(loginPage = "/loginsimple.html",
                                                                          errorPage = "/loginsimplenope.html"))
@ApplicationScoped
public class ApplicationConfig {
}
